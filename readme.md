# Razzle typescript express boilerplate

[![pipeline status](https://gitlab.com/nylsoo/razzle-typescript-express-boilerplate/badges/master/pipeline.svg)](https://gitlab.com/nylsoo/razzle-typescript-express-boilerplate/commits/master)
[![coverage report](https://gitlab.com/nylsoo/razzle-typescript-express-boilerplate/badges/master/coverage.svg)](https://gitlab.com/nylsoo/razzle-typescript-express-boilerplate/commits/master)

Boilerplate for razzle, typescript and a custom express server.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Installing

Clone the repository to your local system.

```
git clone git@gitlab.com:nylsoo/razzle-typescript-express-boilerplate.git PROJECT_DIR
```

Install the dependencies for the boilerplate.

``npm install`` or ``yarn``

## Running the tests

Tests can be run with the following command:
```
npm run test
```

### Break down into end to end tests

Description about end to end tests coming soon.

### And coding style tests

The linters are automatically checked pre-commit. So when the files you changed don't check the tslint.json the linter will throw a error and you will not be able to commit.

To run the linter yourself run the following command:
```
npm run lint
```

or if you want to run a autofixer run the following command:
```
npm run lint-fix
```

## Deployment

Description about deployment coming soon.